﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;
using Moq;
using WaC.Interfaces;
using WaC.Controllers;
using WaC.Models;
using Xunit;
using WaC.Tools.Services;
using WaC.MockDataRepos;
using Microsoft.AspNetCore.Mvc;
using WaC.ViewModels;

namespace WaC_XUnit_Tests.Controller_Tests
{
    public class CharacterController_Tests
    {
        Mock<IMockRepository<ICharListItemVm>> mockCharactersRepo;
        Mock<IConvertService<ICharListItemVm>> mockConvertService;
        CharacterController controller;

        List<ICharListItemVm> mockCharacters = new List<ICharListItemVm>
            {
                new Character{Id=1, MovieId=1, Name="Luke Skywalker", ImageUrl="www.someMovieImageUrl.com"},
                new Character{Id=2, MovieId=1, Name="Obi-Wan Kenobi", ImageUrl="www.someMovieImageUrl.com"},
                new Character{Id=3, MovieId=1, Name="Yoda", ImageUrl="www.someMovieImageUrl.com"}
            };
        List<BaseListVM> mockConverted = new List<BaseListVM>
            {
                new BaseListVM{Id=1, ShortDescription="", Name="Luke Skywalker", ImageUrl="www.someMovieImageUrl.com"},
                new BaseListVM{Id=2, ShortDescription="", Name="Obi-Wan Kenobi", ImageUrl="www.someMovieImageUrl.com"},
                new BaseListVM{Id=3, ShortDescription="", Name="Yoda", ImageUrl="www.someMovieImageUrl.com"}
            };

        // Setup
        public CharacterController_Tests()
        {
            mockCharactersRepo = new Mock<IMockRepository<ICharListItemVm>>();
            mockCharactersRepo.Setup(p => p.GetAll).Returns(mockCharacters);

            mockConvertService = new Mock<IConvertService<ICharListItemVm>>();
            mockConvertService.Setup(p => p.EntityItemToListVM(mockCharacters)).Returns(mockConverted);

            controller = new CharacterController(mockCharactersRepo.Object, mockConvertService.Object);
        }

        [Fact] // List
        public void List()
        {
            // Act
            var actual = (ViewResult)controller.List();
            var listVM = (List<BaseListVM>)actual.Model;

            Assert.Equal(3, listVM.Count);
        }

        [Theory] // Details
        [InlineData(1)]
        [InlineData(2)]
        public void Details(int Id)
        {
            // Arrange
            mockCharactersRepo
                .Setup(o => o.GetById(Id))
                .Returns(mockCharacters.Find(i=> i.Id == Id));

            // Act
            var actual = controller.Details(Id);

            // Assert
            Assert.NotNull(actual);
        }

        [Fact] // Create
        public void Create()
        {
            // Arrange
            // Act
            // Assert
        }

        [Fact] // Add
        public void Add()
        {
            // Arrange
            // Act
            // Assert
        }

        [Fact] // Edit
        public void Edit()
        {
            // Arrange
            // Act
            // Assert
        }

        [Fact] // Update
        public void SubmitEdit()
        {
            // Arrange
            // Act
            // Assert
        }

        [Fact] // Delete
        public void Delete()
        {
            // Arrange
            // Act
            // Assert
        }
    }
}
