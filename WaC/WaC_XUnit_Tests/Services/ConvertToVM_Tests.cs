﻿using Xunit;
using AutoFixture;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.Tools.Services;

namespace WaC_XUnit_Tests.Services
{
    public class ConvertToVM_Tests
    {
        private class BaseClass : IBaseListItemVM
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageUrl { get; set; }
            public string ShortDescription { get; set; }

        };
        
        private class SomeClass : BaseClass
        {
            public string AdditionalStringData { get; set; }
            public int AdditionalIntData { get; set; }
        }

        private SomeClass creator()
        {
            return new SomeClass
            {
                Id = 1,
                Name = "",
                ImageUrl = "",
                ShortDescription = "",
                AdditionalIntData = 1,
                AdditionalStringData = ""
            };

        }

        private List<SomeClass> unformatedList;
        private ConvertToVM<SomeClass> service;

        public ConvertToVM_Tests()
        {
            unformatedList = new List<SomeClass>();
            service = new ConvertToVM<SomeClass>();
        }

        [Fact] // EntityItemToListVM
        public void EntityItemToListVM_WithNonEmptyList_ShouldReturnTheListFormated()
        {
            // Arrange
            unformatedList.AddMany(creator, 10);

            // Act
            var actual = service.EntityItemToListVM(unformatedList);

            // Assert
            Assert.NotSame(unformatedList, actual);
            Assert.True(unformatedList.Count == actual.Count);
        }
    }
}
