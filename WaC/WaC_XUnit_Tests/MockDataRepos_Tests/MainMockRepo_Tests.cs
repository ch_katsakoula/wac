﻿using Xunit;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.MockDataRepos;

namespace WaC_XUnit_Tests.MockDataRepos_Tests
{
    public class MainMockRepo_Tests
    {
        private class BaseListItemVMClass : IBaseListItemVM
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageUrl { get; set; }
            public string ShortDescription { get; set; }
        };

        private BaseListItemVMClass testEntity;
        private MainMockRepo<BaseListItemVMClass> testRepo;

        public MainMockRepo_Tests()
        {
            testEntity = new BaseListItemVMClass
            {
                Id = 1,
                Name = "Entity Name",
                ImageUrl = "www.ImageUrl.com",
                ShortDescription = "ShortDescription"
            };

            testRepo = new MainMockRepo<BaseListItemVMClass>() { List = new List<BaseListItemVMClass> { testEntity } };
        }

        [Fact] // GetById
        public void GetById_WithExistingId_ShouldReturnTheCorrectEntity()
        {
            // Act
            var actual = testRepo.GetById(1);

            // Assert
            Assert.Same(testEntity, actual);
        }

        [Fact] // GetById
        public void GetById_WithNonExistingId_ShouldReturnEntityNotFound()
        {
            // Act
            var actual = testRepo.GetById(2);

            // Assert
            Assert.Null(actual);
        }


        [Fact] // Delete
        public void Delete_WithExistingId_ShouldDeleteCorrectEntity()
        {
            // Act
            testRepo.Delete(1);
            var actual = testRepo.List.Exists(i => i.Id == 1);

            // Assert
            Assert.False(actual);
        }

        [Fact] // Delete
        public void Delete_WithNonExistingId_ShouldNotDeleteAnyEntity()
        {
            // Act
            int expected = testRepo.List.Count;
            testRepo.Delete(2);
            int actual = testRepo.List.Count;

            // Assert
            Assert.True(actual == expected);
        }

        [Fact] // Post
        public void Post_WithNewEntity_ShouldAddAtTheEndOfTheList()
        {
            // Arrange
            BaseListItemVMClass newEntity = new BaseListItemVMClass
            {
                Id = 0,
                Name = "New Entity Name",
                ImageUrl = "www.NewImageUrl.com",
                ShortDescription = "NewShortDescription"
            };

            // Act
            testRepo.Post(newEntity);
            var expected = testRepo.List[testRepo.List.Count - 1];

            // Assert
            Assert.Equal(newEntity, expected);

        }

        [Fact] // Post
        public void Post_WithExistingEntity_ShouldNotAddAtTheList()
        {
            // Arrange
            BaseListItemVMClass newEntity = new BaseListItemVMClass { Id = 1, Name = "", ImageUrl = "", ShortDescription = "" };

            // Act
            int expected = testRepo.List.Count;
            testRepo.Post(newEntity);
            int actual = testRepo.List.Count;

            // Assert
            Assert.True(actual == expected);
        }

        [Fact] // Put
        public void Put_WithExistingEntity_ShouldUpdateTheCorrectEntity()
        {
            // Arrange
            BaseListItemVMClass expected = new BaseListItemVMClass { Id = 1, Name = "", ImageUrl = "", ShortDescription = "" };
            var beforeUpdate = testRepo.List.Find(i => i.Id == expected.Id);
            Assert.NotEqual(expected, beforeUpdate);

            // Act
            testRepo.Put(expected);
            var actual = testRepo.List.Find(i => i.Id == expected.Id);

            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact] // Put
        public void Put_WithNewEntity_ShouldNotUpdateAnyEntity()
        {
            // Arrange
            BaseListItemVMClass nonExistingEntity = new BaseListItemVMClass { Id = 2, Name = "", ImageUrl = "", ShortDescription = "" };

            // Act
            var expected = testRepo.List;
            testRepo.Put(nonExistingEntity);
            var actual = testRepo.List;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
