﻿using NUnit.Framework;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.MockDataRepos;

namespace WaC_Tests.MockDataRepos_Tests
{
    class MainMockRepo_Tests
    {
        private class BaseListItemVMClass : IBaseListItemVM
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageUrl { get; set; }
            public string ShortDescription { get; set; }
        };

        private BaseListItemVMClass testEntity;
        private MainMockRepo<BaseListItemVMClass> testRepo;

        [SetUp]
        public void Setup()
        {
            testEntity = new BaseListItemVMClass
            {
                Id = 1,
                Name = "Entity Name",
                ImageUrl = "www.ImageUrl.com",
                ShortDescription = "ShortDescription"
            };

            testRepo = new MainMockRepo<BaseListItemVMClass>() { List = new List<BaseListItemVMClass> { testEntity } };
        }

        [Test] // GetById
        public void GetById_WithExistingId_ShouldReturnTheCorrectEntity()
        {
            // Act
            var actual = testRepo.GetById(1);

            // Assert
            Assert.AreSame(testEntity, actual);
        }

        [Test] // GetById
        public void GetById_WithNonExistingId_ShouldReturnEntityNotFound()
        {
            // Act
            var actual = testRepo.GetById(2);

            // Assert
            Assert.IsNull(actual);
        }


        [Test] // Delete
        public void Delete_WithExistingId_ShouldDeleteCorrectEntity()
        {
            // Act
            testRepo.Delete(1);
            var actual = testRepo.List.Exists(i => i.Id == 1);

            // Assert
            Assert.IsFalse(actual);
        }

        [Test] // Delete
        public void Delete_WithNonExistingId_ShouldNotDeleteAnyEntity()
        {
            // Act
            int expected = testRepo.List.Count;
            testRepo.Delete(2);
            int actual = testRepo.List.Count;

            // Assert
            Assert.IsTrue(actual == expected);
        }

        [Test] // Post
        public void Post_WithNewEntity_ShouldAddAtTheEndOfTheList()
        {
            // Arrange
            BaseListItemVMClass newEntity = new BaseListItemVMClass
            {
                Id = 0,
                Name = "New Entity Name",
                ImageUrl = "www.NewImageUrl.com",
                ShortDescription = "NewShortDescription"
            };

            // Act
            testRepo.Post(newEntity);
            var expected = testRepo.List[testRepo.List.Count - 1];

            // Assert
            Assert.AreEqual(newEntity, expected);

        }

        [Test] // Post
        public void Post_WithExistingEntity_ShouldNotAddAtTheList()
        {
            // Arrange
            BaseListItemVMClass newEntity = new BaseListItemVMClass { Id = 1, Name = "", ImageUrl = "", ShortDescription = "" };

            // Act
            int expected = testRepo.List.Count;
            testRepo.Post(newEntity);
            int actual = testRepo.List.Count;

            // Assert
            Assert.IsTrue(actual == expected);
        }

        [Test] // Put
        public void Put_WithExistingEntity_ShouldUpdateTheCorrectEntity()
        {
            // Arrange
            BaseListItemVMClass expected = new BaseListItemVMClass { Id = 1, Name = "", ImageUrl = "", ShortDescription = "" };
            var beforeUpdate= testRepo.List.Find(i => i.Id == expected.Id);
            Assert.AreNotEqual(expected, beforeUpdate);

            // Act
            testRepo.Put(expected);
            var actual = testRepo.List.Find(i => i.Id == expected.Id);

            // Assert
            Assert.AreEqual(expected, actual);
        }


        [Test] // Put
        public void Put_WithNewEntity_ShouldNotUpdateAnyEntity()
        {
            // Arrange
            BaseListItemVMClass nonExistingEntity = new BaseListItemVMClass { Id = 2, Name = "", ImageUrl = "", ShortDescription = "" };

            // Act
            var expected = testRepo.List;
            testRepo.Put(nonExistingEntity);
            var actual = testRepo.List;

            // Assert
            Assert.AreEqual(expected, actual);

        }


    }
}
