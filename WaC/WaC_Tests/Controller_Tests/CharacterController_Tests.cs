﻿using System;
using System.Text;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.Models;
using WaC.Controllers;
using WaC.MockDataRepos;
using Moq;
using Xunit;

namespace WaC_Tests.Controller_Tests
{
    class CharacterController_Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test] // List
        public void List()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Details
        public void Details()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Create
        public void Create()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Add
        public void Add()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Edit
        public void Edit()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Update
        public void SubmitEdit()
        {
            // Arrange
            // Act
            // Assert
        }

        [Test] // Delete
        public void Delete()
        {
            // Arrange
            // Act
            // Assert
        }
    }
}
