﻿using AutoFixture;
using NUnit.Framework;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.Tools.Services;

namespace WaC_Tests.Services
{
    class ConvertToVM_Tests
    {
        private class BaseClass : IBaseListItemVM
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ImageUrl { get; set; }
            public string ShortDescription { get; set; }

        };
        private class SomeClass : BaseClass
        {
            public string AdditionalStringData { get; set; }
            public int AdditionalIntData { get; set; }
        }


        private SomeClass creator()
        {
            return new SomeClass
            {
                Id = 1,
                Name = "",
                ImageUrl = "",
                ShortDescription = "",
                AdditionalIntData = 1,
                AdditionalStringData = ""
            };

        }

        private List<SomeClass> unformatedList;
        private ConvertToVM<SomeClass> service;

        [SetUp]
        public void Setup()
        {
            unformatedList = new List<SomeClass>();
            service = new ConvertToVM<SomeClass>();
        }

        [Test] // EntityItemToListVM
        public void EntityItemToListVM_WithNonEmptyList_ShouldReturnTheListFormated()
        {
            // Arrange
            unformatedList.AddMany(creator, 10);

            // Act
            var actual = service.EntityItemToListVM(unformatedList);

            Assert.AreNotEqual(unformatedList, actual);
            Assert.IsTrue(unformatedList.Count == actual.Count);
        }
    }
}
