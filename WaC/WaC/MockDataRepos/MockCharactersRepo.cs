﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.ViewModels;
using WaC.Models;

namespace WaC.MockDataRepos
{
    public class MockCharactersRepo : MainMockRepo<ICharListItemVm>
    {
        public MockCharactersRepo()
        {
            List = new List<ICharListItemVm>
            {
                new Character{Id=1, MovieId=1, Name="Luke Skywalker", ImageUrl="https://img.maximummedia.ie/joe_ie/eyJkYXRhIjoie1widXJsXCI6XCJodHRwOlxcXC9cXFwvbWVkaWEtam9lLm1heGltdW1tZWRpYS5pZS5zMy5hbWF6b25hd3MuY29tXFxcL3dwLWNvbnRlbnRcXFwvdXBsb2Fkc1xcXC8yMDE0XFxcLzA3XFxcL2x1a2Vza3l3YWxrZXIuanBnXCIsXCJ3aWR0aFwiOjc2NyxcImhlaWdodFwiOjQzMSxcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvd3d3LmpvZS5pZVxcXC9hc3NldHNcXFwvaW1hZ2VzXFxcL2pvZVxcXC9uby1pbWFnZS5wbmc_aWQ9MjY0YTJkYmUzNzBmMmM2NzVmY2RcIixcIm9wdGlvbnNcIjpbXX0iLCJoYXNoIjoiMzY5YzZiNDMzMDUxZGQzMmU5MjgzNmY2MGIxMWQ4ZWQzMjQ4YzllMCJ9/lukeskywalker.jpg"},
                new Character{Id=2, MovieId=1, Name="Obi-Wan Kenobi", ImageUrl="https://d26oc3sg82pgk3.cloudfront.net/files/media/edit/image/33482/article_full%401x.jpg" },
                new Character{Id=3, MovieId=1, Name="Yoda", ImageUrl="https://miro.medium.com/max/3072/0*bv4qdUZKu9aqt15g.jpg" },
                new Character{Id=4, MovieId=1, Name="Princess Leia", ImageUrl="https://am22.akamaized.net/tms/cnt/uploads/2017/08/leiatop1-650x574.jpg" },
                new Character{Id=5, MovieId=1, Name="Han Solo", ImageUrl="https://lumiere-a.akamaihd.net/v1/images/han-solo-main_a4c8ff79.jpeg?region=0%2C0%2C1920%2C1080&width=960" },
                new Character{Id=6, MovieId=1, Name="Chewbacca", ImageUrl="https://cache.escapistmagazine.com/2019/06/04093446/joonas-suotamo-chewbacca-bts-5.jpg" },
                new Character{Id=7, MovieId=1, Name="R2-D2", ImageUrl="https://lumiere-a.akamaihd.net/v1/images/r2-d2_41dacaa9_68566da1.jpeg?region=0%2C0%2C1536%2C864&width=960" },
                new Character{Id=8, MovieId=1, Name="C-3PO", ImageUrl="https://img.cinemablend.com/filter:scale/quill/7/c/f/8/0/7/7cf807bf58f88c639eddc8fbe6fa65fb9463d9bd.png?mw=600" },
                new Character{Id=9, MovieId=1, Name="2-1B", ImageUrl="https://lumiere-a.akamaihd.net/v1/images/2-1b-droid-main-image_546a90ad.jpeg?region=0%2C107%2C1560%2C880&width=960" },
                new Character{Id=10, MovieId=1, Name="Darth Sidious", ImageUrl="https://img1.looper.com/img/gallery/the-most-powerful-sith-in-the-star-wars-universe/intro.jpg" },
                new Character{Id=11, MovieId=1, Name="Darth Vader",  ImageUrl="https://img.maximummedia.ie/her_ie/eyJkYXRhIjoie1widXJsXCI6XCJodHRwOlxcXC9cXFwvbWVkaWEtaGVyLm1heGltdW1tZWRpYS5pZS5zMy5hbWF6b25hd3MuY29tXFxcL3dwLWNvbnRlbnRcXFwvdXBsb2Fkc1xcXC8yMDE5XFxcLzAxXFxcLzAxMDk0MTQ4XFxcL3JvZ3VlLW9uZS1kYXJ0aC12YWRlci0wMi5qcGdcIixcIndpZHRoXCI6NzY3LFwiaGVpZ2h0XCI6NDMxLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC93d3cuaGVyLmllXFxcL2Fzc2V0c1xcXC9pbWFnZXNcXFwvaGVyXFxcL25vLWltYWdlLnBuZz9pZD1iNmY4NGQ2MjdiNDExNGYwMGY1MFwiLFwib3B0aW9uc1wiOltdfSIsImhhc2giOiJmODczMDQ0NTc3MWVhZDM3MjJmZmU1MzEyNjJlYTVjZDVhM2Y4M2MxIn0=/rogue-one-darth-vader-02.jpg" },
                new Character{Id=12, MovieId=1, Name="Darth Maul",   ImageUrl="https://www.shoptoyco.com/wp-content/uploads/2019/01/Darth-Maul.jpg" },
            };
        }
    }
}
