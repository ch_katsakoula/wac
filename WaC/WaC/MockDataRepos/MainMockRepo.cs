﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaC.Interfaces;
using WaC.Models;
using WaC.ViewModels;

namespace WaC.MockDataRepos
{
    public class MainMockRepo<T> : IMockRepository<T> where T : IBaseListItemVM
    {
        public List<T> List { get; set; }

        public List<T> GetAll => List;

        public T GetById(int Id) => List.Find(item => item.Id == Id);

        public void Delete(int Id)
        {
            int index = List.FindIndex(i => i.Id == Id);
            if (index >= 0)
            {
                List.RemoveAt(index);
            }
        }

        public void Post(T item)
        {
            if (item.Id == 0)
            {
                item.Id = List.Count() + 1;
                List.Add(item);
            }
        }

        public void Put(T item)
        {
            int index = List.FindIndex(i => i.Id == item.Id);
            if (index >= 0)
            {
                List.Insert(index, item);
            }
        }

    }
}
