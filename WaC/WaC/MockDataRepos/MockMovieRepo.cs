﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaC.Interfaces;
using WaC.ViewModels;
using WaC.Models;

namespace WaC.MockDataRepos
{
    public class MockMoviesRepo : MainMockRepo<IMovieListItemVm>
    {
        public MockMoviesRepo()
        {
            List = new List<IMovieListItemVm>
            {
                new Movie{ Id=1, Name="Star Wars", ImageUrl="https://media.comicbook.com/2019/08/star-wars-1184496-1280x0.jpeg", ShortDescription="sd", Description="D"},
                new Movie{ Id=2, Name="Lord of the Rings",ImageUrl="https://nerdist.com/wp-content/uploads/2019/07/LordOfTheRings-1200x676.jpg", ShortDescription="sd", Description="D"},
                new Movie{ Id=3, Name="All the others", ImageUrl="https://img.cinemablend.com/cb/9/6/a/d/5/1/96ad5159b98e9a4a02feb5dca5d4a0fabf73a7c01ae797f6b7c4372d9d735ac0.jpg", ShortDescription="sd", Description="D"},
            };
        }
    }

}
