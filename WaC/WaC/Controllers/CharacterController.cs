﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WaC.Interfaces;
using WaC.Models;
using WaC.ViewModels;

namespace WaC.Controllers
{
    public class CharacterController : Controller
    {
        private readonly IMockRepository<ICharListItemVm> _characterMockRepository;
        private readonly IConvertService<ICharListItemVm> _convertToVM;
        public List<BaseListVM> charactersListVM;

        public CharacterController(IMockRepository<ICharListItemVm> characterMockRepository, IConvertService<ICharListItemVm> convertToVM)
        {
            _characterMockRepository = characterMockRepository;
            _convertToVM = convertToVM;
        }

        // GetAll: Open List
        public ViewResult List()
        {
            var charList = _characterMockRepository.GetAll;
            charactersListVM = _convertToVM.EntityItemToListVM(charList);

            return View(charactersListVM);
        }

        // GetById: Open Details
        public ViewResult Details(int Id)
        {
            return View(_characterMockRepository.GetById(Id));
        }

        // Open Create Form
        public ViewResult Create()
        {
            return View();
        }

        // Create
        public ActionResult Add(Character character)
        {
            _characterMockRepository.Post(character);
            return RedirectToAction("List");
        }

        // Open Edit Form
        public ViewResult Edit(int Id)
        {
            return View(_characterMockRepository.GetById(Id));
        }

        // Update
        public ActionResult SubmitEdit(Character character)
        {
            _characterMockRepository.Put(character);
            return RedirectToAction("List");
        }


        // Delete
        public ActionResult Delete(int Id)
        {
            _characterMockRepository.Delete(Id);
            return RedirectToAction("List");
        }

    }
}