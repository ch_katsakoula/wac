﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WaC.Interfaces;
using WaC.Models;
using WaC.ViewModels;
using WaC.Tools.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WaC.MockDataRepos;

namespace WaC.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMockRepository<IMovieListItemVm> _moviesMockRepository;
        private readonly IConvertService<IMovieListItemVm> _convertToVM;
        public List<BaseListVM> moviesListVM;

        public MovieController(IMockRepository<IMovieListItemVm> characterMockRepository, IConvertService<IMovieListItemVm> convertToVM)
        {
            _moviesMockRepository = characterMockRepository;
            _convertToVM = convertToVM;
        }

        public ViewResult List()
        {
            moviesListVM = _convertToVM.EntityItemToListVM(_moviesMockRepository.GetAll);

            return View(moviesListVM);
        }

        public ViewResult Details(int Id)
        {
            return View();
        }

    }
}