using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WaC.MockDataRepos;
using WaC.Models;
using WaC.ViewModels;
using WaC.Interfaces;
using WaC.Tools.Services;

namespace WaC
{
    public class Startup
    {
        /* 5 */
        /* Only the following service types can be injected into the Startup constructor when 
         * using the Generic Host (IHostBuilder):
         * IWebHostEnvironment
         * IHostEnvironment
         * IConfiguration */
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        /* 6 */
        /* The StartUp class optionally includes a ConfigureServices method to configure the app's services. 
          A service is a reusable component that provides app functionality. Services are 
          registered in ConfigureServices and consumed across the app via dependency injection (DI)
          or ApplicationServices.
          Adding services to the service container makes them available within the app and in the 
          Configure method.*/
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddScoped<IBaseListItemVM, BaseListVM>();
            services.AddScoped<IConvertService<ICharListItemVm>, CharacterConvertToVM<ICharListItemVm>>();
            services.AddScoped<IConvertService<IMovieListItemVm>, MovieConvertToVM<IMovieListItemVm>>();
            services.AddSingleton<IMockRepository<ICharListItemVm>, MockCharactersRepo>();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            /* 7 */
            /* The StartUp class always includes a Configure method to create the app's request processing
              pipeline.
              The Configure method is used to specify how the app responds to HTTP requests. The request 
              pipeline is configured by adding middleware components to an IApplicationBuilder instance. 
              IApplicationBuilder is available to the Configure method, but it isn't registered in the service 
              container. Hosting creates an IApplicationBuilder and passes it directly to Configure.
              * By convention, a middleware component is added to the pipeline by invoking its Use... extension
                method in the Startup.Configure method. Each Use extension method adds one or more middleware 
                components to the request pipeline.
              * Each middleware performs asynchronous operations on an HttpContext and then either invokes 
                the next middleware in the pipeline or terminates the request.
              * Additional services, such as IWebHostEnvironment, ILoggerFactory, or anything defined in 
                ConfigureServices, can be specified in the Configure method signature. These services are
                injected if they're available.*/
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
