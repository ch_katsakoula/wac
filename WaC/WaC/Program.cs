using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WaC
{
    public class Program
    {
        /* No 1 */
        /* An ASP.NET Core app builds a host on startup.The host is an object that encapsulates 
         * all of the app's resources, such as:
         * An HTTP server implementation
         * Middleware components
         * Logging
         * DI
         * Configuration
        */

        public static void Main(string[] args)
        {
            /* No 2 */
            /* The code to create a host is in Program.Main */
            CreateHostBuilder(args).Build().Run();
        }

        /* No 2 */
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            /* No 3 */
            /* The CreateDefaultBuilder and ConfigureWebHostDefaults methods configure a host 
             * with commonly used options
             */
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    /* No 4 */
                    /* The Startup class is specified when the app's host is built. The Startup class is 
                     * typically specified by calling the WebHostBuilderExtensions.UseStartup<TStartup> 
                     * method on the host builder. 
                     * The host provides services that are available to the Startup class constructor
                     */
                    webBuilder.UseStartup<Startup>();
                });
    }
}
