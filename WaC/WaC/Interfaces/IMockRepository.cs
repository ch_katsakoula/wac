﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaC.Interfaces;
using WaC.Models;

namespace WaC.Interfaces
{
    public interface IMockRepository<T> where T : IBaseListItemVM
    {
        public List<T> List { get; set; }
        public List<T> GetAll { get; }
        public T GetById(int Id);
        public void Delete(int Id);
        public void Post(T item);
        public void Put(T item);

    }
}
