﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaC.ViewModels;

namespace WaC.Interfaces
{
    public interface IConvertService<TEntity> where TEntity : IBaseListItemVM
    {
        public List<BaseListVM> EntityItemToListVM(List<TEntity> entities);
    }
}
