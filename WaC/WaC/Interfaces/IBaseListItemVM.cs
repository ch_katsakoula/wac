﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaC.Interfaces
{
    public interface IBaseListItemVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ShortDescription { get; set; }
    }

    public interface ICharListItemVm : IBaseListItemVM { }
    public interface IMovieListItemVm : IBaseListItemVM { }


}
