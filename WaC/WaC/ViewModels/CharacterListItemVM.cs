﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaC.Models;
using WaC.Interfaces;

namespace WaC.ViewModels
{
    public class CharacterListItemVM: BaseListVM
    {
        public string MovieName { get; set; }
    }
}
