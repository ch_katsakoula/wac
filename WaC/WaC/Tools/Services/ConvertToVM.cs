﻿using System.Collections.Generic;
using System.Linq;
using WaC.Interfaces;
using WaC.ViewModels;

namespace WaC.Tools.Services
{
    public class ConvertToVM<TEntity> : IConvertService<TEntity>
        where TEntity : IBaseListItemVM
    {
        public List<BaseListVM> EntityItemToListVM(List<TEntity> entities)
        {
            var ent = entities;
            if (entities.Count > 0)
            {
                return entities.Select(i => new BaseListVM()
                {
                    Id = i.Id,
                    Name = i.Name,
                    ImageUrl = i.ImageUrl,
                    ShortDescription = i.ShortDescription
                }).ToList();
            }
            else
            {
                return new List<BaseListVM>();
            }
        }
    }

    public class CharacterConvertToVM<TEntity> : ConvertToVM<TEntity> where TEntity : ICharListItemVm { }
    public class MovieConvertToVM<TEntity> : ConvertToVM<TEntity> where TEntity : IMovieListItemVm { }


}
