﻿using System.Collections.Generic;
using WaC.Interfaces;

namespace WaC.Models
{
    public class Movie : IMovieListItemVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public int ReleasedYear { get; set; }
        public List<Character> Characters { get; set; }
    }
}
