﻿using WaC.Interfaces;

namespace WaC.Models
{
    public class Character : ICharListItemVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public int MovieId { get; set; }
    }
}
